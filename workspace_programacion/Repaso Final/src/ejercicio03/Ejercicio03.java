package ejercicio03;
import java.util.Scanner;
public class Ejercicio03 {
	static final String FINAL = "Final Del Programa";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input =  new Scanner(System.in);
		
		System.out.println("Introduce un numero");
		int num  = input.nextInt();
		
		do {
			System.out.println(num--);
		}while(num>=1);
		
		System.out.println(FINAL);
		
		input.close();

	}

}
