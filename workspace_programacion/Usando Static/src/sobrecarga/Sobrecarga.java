package sobrecarga;

public class Sobrecarga {

	public static void demoSobrecarga() {
		System.out.println("Metodo sobrecargado sin parametros");
	}

	public static void demoSobrecarga(int a) {
		System.out.println("Metodo con dos parametros" + a);
	}

	public static int demoSobrecarga(int a, int b) {
		System.out.println("Metodo con dos parametros" + a + "y" + b);
		return a + b;
	}

	public static double demoSobrecarga(double a, double b) {
		System.out.println("Metodo con dos parametros" + a + "y" + b);
		return a + b;
	}

	public static void main(String ags[]) {
		System.out.println("Metodo : ");
		Sobrecarga.demoSobrecarga();

		System.out.println("Metodo 2");
		Sobrecarga.demoSobrecarga(3);

		System.out.println("Metodo 3");
		Sobrecarga.demoSobrecarga(2, 3);
		System.out.println();

		System.out.println("Metodo 4");
		double y = Sobrecarga.demoSobrecarga(4.2, 3.4);
		System.out.println(y);
	}

}
