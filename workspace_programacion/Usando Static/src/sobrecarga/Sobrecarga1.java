package sobrecarga;

public class Sobrecarga1 {

	public static void demoSobrecarga() {
		System.out.println("Metodo sobrecargado sin parametros");
	}

	public static void demoSobrecarga(int a) {
		System.out.println("Metodo con dos parametros" + a);
	}

	public static int demoSobrecarga(int a, int b) {
		System.out.println("Metodo con dos parametros" + a + "y" + b);
		return a + b;
	}

	public static double demoSobrecarga(double a, double b) {
		System.out.println("Metodo con dos parametros" + a + "y" + b);
		return a + b;
	}

	public static void main(String ags[]) {
		
		Sobrecarga1 objetoSobrecarga = new Sobrecarga1();
		
		System.out.println("Metodo : ");
		objetoSobrecarga.demoSobrecarga();

		System.out.println("Metodo 2");
		objetoSobrecarga.demoSobrecarga(3);

		System.out.println("Metodo 3");
		objetoSobrecarga.demoSobrecarga(2, 3);
		System.out.println();

		System.out.println("Metodo 4");
		double y = objetoSobrecarga.demoSobrecarga(4.2, 3.4);
		System.out.println(y);
	}

}
