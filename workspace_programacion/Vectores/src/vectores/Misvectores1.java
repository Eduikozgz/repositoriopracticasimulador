package vectores;

import java.util.Scanner;

public class Misvectores1 {

	public static void main(String[] args) {
		// declaramos un vector

		int mivector[] = new int[10];
		int[] mivector1 = new int[10];

		// declaramos un scanner

		Scanner input = new Scanner(System.in);
		// pedimos los datos
		for (int i = 0; i < 10; i++) {
			System.out.println("Dame la componente" + i + "del vector");
			mivector[i] = input.nextInt();
		}
		// mostrar los datos
		for (int i = 0; i < 10; i++) {
			System.out.println("La componente" + i + "del vector es:" + mivector[i]);
		}

		// sumamoslos datos
		int suma = 0;
		for (int i = 0; i < 10; i++) {
			suma += mivector[i];
		}
		System.out.println("La suma es:" + suma);
		input.close();
		//igualo vectores
		mivector1=mivector;
		//muestro los datos
		for (int i = 0; i < 10; i++) {
			System.out.println("La componente" + i + "del vector es:" + mivector1[i]);
		}

	}

}
