package ejercicio02;
import java.util.Scanner;
public class Ejercicio02 {
	static final String	FINAL = "FINAL DEL PROGRAMA";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int num = 100;  
		while(num >=1) {
			
			System.out.println(num--);
			
		}
		
		int num2 = 100;  
		while(num2 >=1) {
			
			System.out.print(num2-- +" ");
			
		}
		
		System.out.println();
		System.out.println(FINAL);

		input.close();

	}

}
