package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		int contador = 0;
		int suma = 0;

		int numero;
		do {
			System.out.println("Introduce numeros enteros");
			numero = input.nextInt();
			
			// almaceno la suma
			suma += numero;

			// cuento los numeros excepto el cero
			if (numero != 0) {
				contador++;
			}

		} while (numero != 0);

		System.out.println("suma: " + suma);
		System.out.println("cantidad: " + contador);

		input.close();

	}

}
