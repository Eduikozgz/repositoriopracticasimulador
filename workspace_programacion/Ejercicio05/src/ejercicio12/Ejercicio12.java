package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int numero;

		do {

			System.out.println("1- Opcion 1");
			System.out.println("2- Opcion 2");
			System.out.println("3- Salir");
			System.out.println("Introduce una opcion");
			numero = input.nextInt();

			switch (numero) {
			case 1:
				System.out.println("Opcion 1 seleccionada");
				break;
			case 2:
				System.out.println("Opcion 2 seleccionada");
				break;
			case 3:
				System.out.println("Adios");
				break;

			default:
				System.out.println("Opcion incorrecta");
				break;
			}

		} while (numero != 3);

		input.close();

	}

}
