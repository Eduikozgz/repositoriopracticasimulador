package ej9;

import java.util.Scanner;

public class Ej9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int Divisor = 23;
		
		Scanner input = new Scanner(System.in);
		System.out.println("Dame los numeros del DNI");
		int DNI = input.nextInt();
		int res= DNI%Divisor;
		
		char letra=letraNif(res);
		System.out.println("Tu letra del DNI es:"+letra);
		
		input.close();
	}
	public static char letraNif(int res) {
		char letrasNif[]= {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
		return letrasNif[res];
}
}
