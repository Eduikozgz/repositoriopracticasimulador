package ejercicio06;
import java.util.Scanner;
public class Ejercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un caracter");
		char caracter = input.nextLine().charAt(0);
		 
		if(caracter >= '0'&& caracter<='9') {
			System.out.println("Es una cifra");
		}else {
			System.out.println("No es una cifra");
		}
			
		
		input.close();

	}

}
