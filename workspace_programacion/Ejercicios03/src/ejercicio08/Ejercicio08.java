package ejercicio08;
import java.util.Scanner;
public class Ejercicio08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Escriba el dividendo");
		double num1 = input.nextDouble();
		System.out.println("Escriba el divisor");
		double num2 = input.nextDouble();
		
		if(num2 !=0 ){
			System.out.println(num1/num2);
		}else {
			System.out.println("No se puede efectuar dicha operacion ya que el divisor es 0 y la solucion seria infinito");
		}
		
		input.close();

	}

}
  