package ejercicio05;
import java.util.Scanner;
public class Ejercicio05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un letra");
		char caracter1 = input.nextLine().charAt(0);
		System.out.println("Escribe otro letra");
		char caracter2 = input.nextLine().charAt(0);
		
		if((caracter1>='a'&&caracter1 <='z')==(caracter2>='a'&&caracter2 <='z')) {
			System.out.println("Los dos  letras son minusculas");
		}else {
			System.out.println("Los dos letras no son minusculas ");
		}
		
		
		input.close();

	}

}
