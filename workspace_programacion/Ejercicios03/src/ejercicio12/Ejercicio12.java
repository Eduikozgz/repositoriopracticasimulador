package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Escriba un mes de forma numerica(ejemplo:mayo =5)");
		int num1 = input.nextInt();

		if (num1 == 1 || num1 == 3 || num1 == 5 || num1 == 7 || num1 == 8 || num1 == 10 || num1 == 12) {
			System.out.println("El mes tiene 31 dias");
		} else {
			if (num1 == 4 || num1 == 6 || num1 == 9 || num1 == 11) {
		
				System.out.println("El mes tiene 30 dias");
			} else {
				if (num1 == 2) {
					System.out.println("El mes tiene 28 o 29 dias");
				} else {
					System.out.println("Ese numero no se corresponde con un mes");
				}
			}
		}
	
		input.close();

	}

}
