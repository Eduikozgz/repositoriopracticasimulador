package ejercicio04;
import java.util.Scanner;
public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Escribe un caracter");
		char caracter1 = input.nextLine().charAt(0);
		System.out.println("Escribe otro caracter");
		char caracter2 = input.nextLine().charAt(0);
		
		if(caracter1==caracter2) {
			System.out.println("Los caracteres son iguales");
		}else {
			System.out.println("Los caracteres no son iguales");
		}
		
		input.close();

	}

}
