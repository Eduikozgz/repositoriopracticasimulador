package ejercicio13;
import java.util.Scanner;
public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena1 = input.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2 = input.nextLine();
		
		if(cadena1.equals(cadena2)) {
			System.out.println("Mismos caracteres - Completamente identicas ");

		} else {
			
			if(cadena1.equalsIgnoreCase(cadena2)) {
				System.out.println("Mismas letras, Diferencia entre mayusculas y minusculas");
		
			} else {
				System.out.println("No son iguales");
			}	
		}
		input.close();
	}
}

