package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un valor int");
		int intt = input.nextInt();
		
		System.out.println("Escribe un valor boolean");
		boolean booleann = input.nextBoolean();
		
		System.out.println("Escribe un valor double	");
		double doublee = input.nextDouble();
		
		System.out.println("Escribe un valor short");
		short shortt = input.nextShort();
		
		System.out.println("Escribe un valor byte");
		byte bytee = input.nextByte();
		
		System.out.println("Escribe un valor long");
		long longg = input.nextLong();
		
		System.out.println("Escribe un valor float");
		float floatt = input.nextFloat();
		
		input.close();
		

	}

}
