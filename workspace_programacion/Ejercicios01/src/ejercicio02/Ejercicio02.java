package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
	int entero = 20;
    char caracter = 'V';
    double decimal = 15.9;
    short numeroShort = 2;
    long enteroLargo = 30L;
    float realSimple = 5.69F;
    byte numeroByte = 120;
    
    System.out.println("ahora muestro las el valor de las variables");
    System.out.println(entero);
    System.out.println(decimal);
    System.out.println(caracter);
    System.out.println(numeroShort);
    System.out.println(enteroLargo);
    System.out.println(realSimple);
    System.out.println(numeroByte);
    System.out.println((int)caracter);
    
    System.out.println("ahora hago operaciones");
    System.out.println(entero + caracter);
    System.out.println(decimal*numeroShort);
    System.out.println(enteroLargo-realSimple);
    System.out.println(entero/numeroByte);
}

}
