package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un valor para el radio");
		double radio = input.nextDouble();
		
		System.out.println("Aqui voy a mostrar la longitud de la circunferencia");
		System.out.println(radio*2*3.14);
		
		System.out.println("Aqui voy a mostrar el area de la circunferencia");
		System.out.println(radio*radio*3.14);
		
		input.close();
		
		

	}

}
