package ejercicio05;
import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1 = input.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2 = input.nextLine();
		
		System.out.println(cadena1.equals(cadena2) ? "Cadenas iguales" : "Cadenas no iguales");
	


		
		input.close();

	}

}
