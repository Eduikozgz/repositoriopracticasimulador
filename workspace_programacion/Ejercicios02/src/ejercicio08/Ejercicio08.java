package ejercicio08;
import java.util.Scanner;
public class Ejercicio08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un numero de 5 cifras");
		int num = input.nextInt();
		System.out.println(num / 10000);
		System.out.println(num / 1000);
		System.out.println(num / 100);
		System.out.println(num / 10);
		System.out.println(num / 1);
		
		input.nextLine();
		
		System.out.println("Escribe un numero de 5 cifras");
		String num1 = input.nextLine();
		System.out.println(num1.charAt(0));
		System.out.println(num1.charAt(1));
		System.out.println(num1.charAt(2));
		System.out.println(num1.charAt(3));
		System.out.println(num1.charAt(4));
		
		
		input.close();

	}

}
